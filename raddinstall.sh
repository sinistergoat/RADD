#!/bin/bash
#
#Checking for internet connection.
echo "Have you connected this device to the internet? (yes/no)"
echo =========================================================================
read answer
if [[ $answer == no ]]; then
	echo "Please connect to the internet"
	exit 1
elif [[ $answer == yes ]]; then
	echo "Proceeding"
else
	echo "Please answer 'yes' or 'no'"
    exit 1
fi
#Checking running directory
echo "Are you running this script from the "RADD" directory? (yes/no)"         
echo =========================================================================
read answer                                                             
if [[ $answer == yes ]]; then
    echo "Proceeding"
elif [[ $answer == no ]]; then
    echo "Please run this script from the "RADD" directory."
    exit 1
else
    echo "Please answer "yes" or "no""
    exit 1
fi
#updating
apt-get update --fix-missing -y
#Installing autossh
apt-get install autossh -y
#Copying script for jumpbox autoconnect
cp ./jumpbox.sh /usr/local/bin/
#Creating variables to use with config files. 
echo "What is your autossh port? "
read autossh
echo "what is your ssh port? " 
read sshport
echo "What is your listen port? "
read listen_port
#Copying autossh service.
cp ./autossh.service /etc/systemd/system/
#making jumpbox script executable
chmod +x /usr/local/bin/jumpbox.sh
#installing crontab
(crontab -l ; echo "#@reboot sleep 30 && /usr/local/bin/jumpbox.sh") | crontab -
# The crontab script returns the error "no crontab for root"
# May turn into bug
mkdir -p ~/.ssh/keys 
cat ./raddconfig >> ~/.ssh/config
echo
#Editing config files
sed -i "s/<mport>/$autossh/g" /usr/local/bin/jumpbox.sh
sed -i "s/<lport>/$listen_port/g" /usr/local/bin/jumpbox.sh
sed -i "s/<sport>/$sshport/g" /usr/local/bin/jumpbox.sh
sed -i "s/<mport>/$autossh/g" /etc/systemd/system/autossh.service
sed -i "s/<lport>/$listen_port/g" /etc/systemd/system/autossh.service
sed -i "s/<sport>/$sshport/g" /etc/systemd/system/autossh.service
sed -i "s/#Port 22/Port $sshport/g" /etc/ssh/sshd_config
#Enabling autossh as a service
systemctl enable autossh
echo 
#Editing Hostname
echo "What does the hostname need to be?"
read hostname
hostnamectl set-hostname $hostname
echo
#Changing the .ssh config file to use your private key.
sed -i "s/<priv_key>/$hostname/g" /root/.ssh/config 
#Changing password
echo "Changing the password now."
passwd root
echo
sleep 1
#creating a public/private key pair
echo "Creating key pair"
ssh-keygen -t ecdsa -b 384 <<Q
/root/.ssh/keys/$hostname


Q
echo "Preparing to push public key to the server, You will need the password."
sleep 5
echo "What is do you want the pivot server to be known as?"
read pivot_server
echo "What is the pivot servers username (usually 'root')"
read pivot_user
echo "What is the ip address of the pivot server?"
read pivot_ip
echo "What is the ssh port of the pivot server (default is 22)"
read pivot_ssh
echo

ssh-copy-id -i /root/.ssh/keys/$hostname.pub $pivot_server
echo 
sed -i "s/yoda/$pivot_server/g" /usr/local/bin/jumpbox.sh
sed -i "s/yoda/$pivot_server/g" /etc/systemd/system/autossh.service
sed -i "s/yoda/$pivot_server/g" /root/.ssh/config
sed -i "s/<user>/$pivot_user/g" /root/.ssh/config
sed -i "s/<ip>/$pivot_ip/g" /root/.ssh/config
sed -i "s/<port>/$pivot_ssh/g" /root/.ssh/config

echo
echo "would you like to remove the install files? (y/n)"
read remove                                                             
if [[ $remove == yes ]]; then                                           
    echo "Removing installation directory"                              
    echo sleep 2                                                        
    rm -r ~/Desktop/RADD && echo "done" 
    echo "Opening files to confirm settings"
elif [[ $remove == y ]]; then                                           
    echo "Removing installation directory"                              
    sleep 2                                                             
    rm -r ~/Desktop/RADD && echo "done"
    echo "Opening files to confirm settings"
elif [[ $remove == no ]]; then                                          
    echo "Opening files to confirm settings."                                        
    sleep 2                                                             
elif [[ $remove == n ]]; then                                           
    echo "Opening files to confirm settings."                                        
    sleep 2                                                             
else                                                                    
    echo "Opening files to confirm settings."                                        
    sleep 2                                                             
fi
vim /usr/local/bin/jumpbox.sh
vim /etc/systemd/system/autossh.service
vim ~/.ssh/config
vim /etc/ssh/sshd_config
reboot
