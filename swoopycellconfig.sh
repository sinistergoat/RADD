#!/bin/bash

# Use this and pray to your got that it makes your sim card work.
echo "installing tools"
apt-get install libqmi-utils udhcpc
sleep 2
echo "setting operating mode"
qmicli -d /dev/cdc-wdm0 --dms-set-operating-mode='online'
sleep 2
echo "configuring the interface"
qmicli -d /dev/cdc-wdm0 -w
ip link set wwan0 down
echo 'Y' | sudo tee /sys/class/net/wwan0/qmi/raw_ip
ip link set wwan0 up
sleep 2
echo "Trying to connect to the network, After this command, you should
get connected to the network. If it fails, the chances aren't good."
qmicli -p -d /dev/cdc-wdm0 --device-open-net='net-raw-ip|net-no-qos-header' --wds-start-network="apn='YOUR_APN',username='YOUR_USERNAME',password='YOUR_PASSWORD',ip-type=4" --client-no-release-cid
sleep 2
echo "Configuring IP address"
udhcpc -i wwan0
sleep 1
echo "Hope that worked."
exit 1
